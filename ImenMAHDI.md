Semaine 1 à 2 :
* Développer la partie header avec le navigation bar, le logo et la partie footer. (En position sticky)
* Préparer les sections qui vont être incluses dans le site. ( En utilisant la balise section)
* Modifier le font du body et fixer les tailles de la classe page à 100vh
Semaine 3 à 4 :
* Commencer à travailler sur la partie sports
* Préparer un Swiper (code JavaScript importé ) et modification de sa forme selon notre besoin.
* Ajouter le contenu de chaque swiper card. 
* Modification des couleurs et du contenu
Semaine 5:
* Ajouter les liens aux boutons du navigation bar pour qu'ils ramènent à différentes parties de la page.