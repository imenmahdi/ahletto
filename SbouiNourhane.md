semaine 1 :
Developper la section home page du code html .
voici le code :

 <div class="page homepage" id="Home">
          <div id="website-description">
            <h1><strong>World's Best<br>Sports Website</strong></h1>
            <p><strong> STEP UP YOUR FITNESS CHALLENGE WITH US. </strong></p>
          </div>

        </div>
		
semaine 2 :
developper le code css de la section home page .
/**homepage style start**/
.homepage{
  background: rgb(34,64,89);
  background: linear-gradient(60deg, rgba(34,64,89,1) 50%, rgba(243,244,246,1) 100%);
  background-image: url(images/ft.jpg);
  background-position:centre;
  background-size: cover ;
  display: flex ;
  box-shadow: 0 2px 5px black;
  display: flex;
  justify-content: space-between;
  align-items: center;
}
/**homepage style end**/
semaine 3:
developper la partie html de la section feedback .

 <section class="feedback-page">
		    <div class="container">
		         <form>
				 <fieldset>
				 <legend><strong> Give Your Feedback </strong> </legend>
				     <div class="id">
				         Full Name :<input type="text" placehold="Full Name">
						 <i class="far fa-user"></i>
					 </div>
					 <div class="id">
				         Email address :<input type="email" placehold="Email address">
						 <i class="far fa-envelope"></i>
					</div>
					<textarea cols="12" rows="5" placeholder="Enter your options here..."></textarea>
                                        <button>Send</button>
                                 </fieldset>
			 </form>
		     </div>

        </section>
semaine 4 :
developper le code css de la section feedback .
/**feedback-pagestyle**/
.feedback-page {
	position: absolute;
	height:100vh;
	width:100%;
	background-image:url(img/bc.jpg);
	background-size: cover ;
	background-position : center;
	display : flex;
	min-height : 100vh;
	align-items: center ;
	justify-content : center;
}
.feedback-page .container {
	width : 600px;
	background: #000;
	box-shadow: 0 0 8px rgba(250,250,250,0,6);
	opacity : 0.6;
}
.feedback-page .container form {
	width : 90%;
	text-align: center;
	padding : 25px 20px ;
}
.feedback-page .container form legend {
	padding : 10px 0 ;
	color : white;
	font-size:30px
}
.feedback-page .container form .id {
	position : auto ;
	color:white;
	width:90%;
}
.feedback-page .container form .id .i {
	position: absolute ;
	font-size : 20px;
	top : 50% ;
	right : 0px;
	transform: translateY(-50%);
}
.feedback-page .container form input {
	width : 90% ;
	height: 50px ;
	margin: 4px 0;
	border : 1px solid #5c5c5c ;
	border-radius: 3px ;
	background: #181717 ;
	padding : 0 15px ;
	padding-right : 45px ;
	font-size :20px ;
}
.feedback-page .container form textarea {
	padding: 5px 15px ;
	border : 1px solid #5c5c5c ;
	border-radius: 3px ;
	background: #181717 ;
	padding : 0 15px ;
	font-size :20px ;
	width:90%;
	margin : 4px 0 ;
}
.feedback-page .container form button {
	margin-top:5px;
	border : none ;
	background: #00fff0;
	color:#222;
	padding : 10px 0;
	width : 100%;
	font-size: 20px ;
	font-weight:800;
	cursor: pointer;
	border-radius :3px;
}
.feedback-page .container form input: focus,
.feedback-page .container form textarea:focus{
	border:1px solid #00fff0;
	color:#00fff0;
	transition: all 0.3s ease ;
}
.feedback-page .container form input:focus::placeholder,
.feedback-page .container form textarea:focus::placeholder{
	padding-left: 4px;
	color:#00fff0;
	transition: all 0.3s ease;
}
.feedback-page .container form input:focus + i{
	color:#00fff0;
	transition: all 0.3s ease;
}
/**feedback-pagestyle end**/


/**about us style start**/